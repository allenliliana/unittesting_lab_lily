from parameterized import parameterized, parameterized_class


import pytest


def test_addition():
    x = 5
    y = 6
    assert SimpleCalculator.addition(x, y) == 11


def test_add_two_positive():
    x = 6
    y = 6
    assert SimpleCalculator.addition(x, y) == 12


def test_add_two_negative():
    x = -6
    y = -6
    assert SimpleCalculator.addition(x, y) == -12


def test_add_one_positive_one_negative():
    x = -6
    y = 7
    assert SimpleCalculator.addition(x, y)

def test_subtraction():
    x = -6
    y = -6
    assert SimpleCalculator.subtraction(x, y) == 0


def test_multiplication():
    x = 6
    y = 6
    assert SimpleCalculator.multiplication(x, y) == 36


def test_division():
    x = 6
    y = 6
    assert SimpleCalculator.division(x, y) == 1

def test_division_exception():
    with pytest.raises(Exception):
        assert SimpleCalculator.division(6, 0)
    