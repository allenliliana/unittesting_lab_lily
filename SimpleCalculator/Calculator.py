class SimpleCalculator():

    @staticmethod
    def addition(x, y):
        if x > 255:
            raise Exception('Input cannot be greater than 255.')
        if x < -255:
            raise Exception('Input cannot be less than -255.')
        else:
            return x + y

    @staticmethod
    def subtraction(x, y):
        return x - y


    @staticmethod
    def division(x, y):
        if y == 0:
            raise Exception('Cannot divide by 0.')
        else:
            return x / y


    @staticmethod
    def multiplication(x, y):
        return x * y


if __name__ == '__main__':
     print(SimpleCalculator.addition(60, 6))
     print(SimpleCalculator.subtraction(5, 6))
     print(multiplication(5, 6))
     print(division(5, 0))

